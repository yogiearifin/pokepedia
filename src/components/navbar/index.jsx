import React from "react";
import { cx } from "@emotion/css";
import { navbar } from "./style";
import { Link } from "react-router-dom";

export default function Navbar() {
  return (
    <div className={cx(navbar)}>
      <div className="logo-container">
        <Link to="/" className="link-navbar">
          <h1 className="poke-logo">Pokepedia</h1>
        </Link>
      </div>
      <div className="button-container">
        <Link to="/mypokemon" className="link-navbar">
          <button className="my-pokemon-button">My Pokemon</button>
        </Link>
      </div>
    </div>
  );
}
