import { css } from "@emotion/css";
import pokefont from "../../font/PokemonSolidNormal-xyWR.ttf";

export const headerDetail = css`
  font-size: 50px;
  .span-new {
    color: red;
    font-size: 12px;
  }
`;

export const navbar = css`
  @font-face {
    font-family: "Pokemon";
    src: local("PokemonSolidNormal-xyWR"), url(${pokefont}) format("truetype");
  }
  background-color: #f3f4f5;
  position: fixed;
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  .link-navbar {
    text-decoration: none;
    color: inherit;
  }
  .logo-container {
    padding-left: 5%;
  }
  .poke-logo {
    color: #81d586;
    font-family: Pokemon, sans-serif;
    -webkit-text-stroke-width: 1px;
    -webkit-text-stroke-color: #1d2c5e;
    letter-spacing: 2px;
    margin: 0;
  }
  .button-container {
    padding-right: 5%;
  }
  .my-pokemon-button {
    background-color: #81d586;
    color: white;
    border: none;
    border-radius: 12px;
    font-weight: bold;
    padding: 10px;
    margin-top: 10px;
    &:hover {
      background-color: white;
      color: #81d586;
    }
  }
`;
