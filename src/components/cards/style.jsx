import { css } from "@emotion/css";

export const link = css`
  text-decoration: none;
  color: inherit;
`;

export const container = css`
  @media (min-width: 900px) {
    margin: 0.5%;
  }
  text-align: center;
  font-family: "Exo", sans-serif;
  margin: 3%;
  border: 1px solid black;
  min-height: 155px;
  .text-container {
    text-transform: capitalize;
    padding: 10% 0;
    h5 {
      margin: 0;
      text-transform: capitalize;
    }
  }
  .image-container {
    background-color: #f4f4f4;
    display: flex;
    justify-content: center;
  }
  .owned-count {
    font-size: 10px;
  }
  .release-pokemon {
    background-color: #ed1b24;
    color: white;
    border: none;
    border-radius: 15px;
    margin: 10% 0;
  }
`;
