import { useContext } from "react";
import { Link } from "react-router-dom";
import { cx } from "@emotion/css";
import { container, link } from "./style";
import { PokeContext } from "../../context/context";

export default function Cards(props) {
  const { removePokemon, pokemonData } = useContext(PokeContext);
  const { name, image, type, nickname } = props;
  const owned = pokemonData?.catchList?.filter(
    (item) => item.name === name
  ).length;
  return (
    <div className={cx(container)}>
      <Link className={cx(link)} to={`/pokemon/${name}`}>
        <div>
          <div className="image-container">
            <img src={image} alt={name} />
          </div>
          <div className="text-container">
            <h5>{type === "my-pokemon" ? nickname : name}</h5>
            {owned && type !== "my-pokemon" ? (
              <span className="owned-count">Owned: {owned}</span>
            ) : null}
          </div>
        </div>
      </Link>
      {type === "my-pokemon" ? (
        <button
          className="release-pokemon"
          onClick={() => removePokemon(nickname)}
        >
          Release
        </button>
      ) : null}
    </div>
  );
}
