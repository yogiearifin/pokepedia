import PokemonRoute from "./Routes/Routes";
import { cx } from "@emotion/css";
import { global } from "./global/style";

function App() {
  return (
    <div className={cx(global)}>
      <PokemonRoute />
    </div>
  );
}

export default App;
