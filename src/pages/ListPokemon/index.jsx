import { useContext, useEffect } from "react";
import { PokeContext } from "../../context/context";
import Cards from "../../components/cards";
import { cx } from "@emotion/css";
import { container } from "../../global/style";
import { containerButton } from "./styles";

function ListPokemon() {
  const { pokemonData, getPokemonList, setOffset } = useContext(PokeContext);
  useEffect(() => {
    getPokemonList();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <div className={cx(container)}>
        {pokemonData?.loading
          ? "loading"
          : pokemonData?.listPokemon?.pokemons?.results?.map((item, index) => {
              return <Cards name={item.name} image={item.image} key={index} />;
            })}
      </div>
      <div className={cx(containerButton)}>
        <button
          disabled={pokemonData.offset - 20 < 0}
          onClick={() => setOffset(pokemonData.offset - 20)}
        >
          {"<"}
        </button>
        <button onClick={() => setOffset(pokemonData.offset + 20)}>
          {">"}
        </button>
      </div>
    </>
  );
}

export default ListPokemon;
