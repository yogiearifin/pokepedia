import { css } from "@emotion/css";

export const containerButton = css`
@media (min-width: 900px) {
  padding: 1% 0;
}
  display: flex;
  justify-content: center;
  padding: 10% 0;
  button {
    margin: 0 10%;
    font-size: 2rem;
    border-radius: 22px;
    font-weight: bold;
    border: none;
  }
`;
