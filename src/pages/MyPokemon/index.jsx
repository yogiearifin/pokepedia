import { useContext, useEffect } from "react";
import { cx } from "@emotion/css";
import { container } from "../../global/style";
import { PokeContext } from "../../context/context";
import Cards from "../../components/cards";

export default function MyPokemon() {
  const { pokemonData, getCatchList } = useContext(PokeContext);
  useEffect(() => {
    getCatchList();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <>
      <div className={cx(container)}>
        {pokemonData?.loading ? (
          "loading"
        ) : pokemonData?.catchList?.length ? (
          pokemonData?.catchList?.map((item, index) => {
            return (
              <Cards
                name={item.name}
                nickname={item.nickname}
                image={item.image}
                key={index}
                type={"my-pokemon"}
              />
            );
          })
        ) : (
          <h1>You haven't caught a Pokemon yet :(</h1>
        )}
      </div>
    </>
  );
}
