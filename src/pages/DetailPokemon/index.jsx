import { useQuery } from "@apollo/client";
import { getDetail } from "../../graphql/detail";
import { useParams } from "react-router-dom";
import { containerDetail } from "./style";
import { cx } from "@emotion/css";
import { containerMedium } from "../../global/style";
import { useContext, useState } from "react";
import { PokeContext } from "../../context/context";
import Modals from "../../components/modals";

export default function DetailPokemon() {
  const { pokemonData, setCatchList } = useContext(PokeContext);
  const [status, setStatus] = useState("");
  const [nickname, setNickname] = useState("");
  const { name } = useParams();
  const { data, loading } = useQuery(getDetail, { variables: { name } });
  const catchPokemon = () => {
    const chance = Math.round(Math.random());
    if (chance) {
      setStatus("success");
    } else {
      setStatus("fail");
    }
  };

  const handleSuccess = () => {
    let newPokemonList;
    if (pokemonData.catchList === null) {
      newPokemonList = [];
    } else {
      newPokemonList = pokemonData.catchList;
    }
    if (newPokemonList.find((item) => item.nickname === nickname)) {
      setStatus("duplicate");
    } else {
      newPokemonList.push({
        name: data.pokemon.name,
        image: data.pokemon.sprites.front_default,
        nickname: nickname,
      });
      setCatchList(newPokemonList);
      setStatus("");
      setNickname("");
    }
  };

  return (
    <div className={cx(containerMedium)}>
      {loading ? (
        "Loading..."
      ) : (
        <div className={cx(containerDetail)}>
          <div className="container-image">
            <img
              src={data.pokemon.sprites.front_default}
              alt={data.pokemon.name}
            />
          </div>
          <h1>{data.pokemon.name}</h1>
          <div className="pokemon-list-vertical">
            {data.pokemon.types.map((item, index) => {
              return <span key={index}>{item.type.name}</span>;
            })}
          </div>
          <div className="container-button">
            <button onClick={catchPokemon}>Catch!</button>
          </div>
          <div>
            <h3>Pokemon's Ability: </h3>
            <div className="pokemon-list-vertical">
              {data.pokemon.abilities.map((item, index) => {
                return <span key={index}>{item.ability.name}</span>;
              })}
            </div>
          </div>
          <div>
            <h3>Pokemon's Moveset: </h3>
            <div className="pokemon-list-horizontal">
              <ul>
                {data.pokemon.moves.map((item, index) => {
                  return <li key={index}>{item.move.name}</li>;
                })}
              </ul>
            </div>
          </div>
        </div>
      )}
      <Modals
        show={status === "fail"}
        title={"Failed!"}
        description={`${data?.pokemon?.name} got away! Better luck next time!`}
      >
        <div>
          <button onClick={() => setStatus("")}>OK</button>
        </div>
      </Modals>
      <Modals
        show={status === "duplicate"}
        title={"Duplicate Name Found!"}
        description={`you already have a ${data?.pokemon?.name} named ${nickname}! Please choose a different name!`}
      >
        <div>
          <button
            onClick={() => {
              setStatus("success");
              setNickname("");
            }}
          >
            OK
          </button>
        </div>
      </Modals>
      <Modals
        show={status === "success"}
        title={"Success!"}
        description={`Congrats! You caught a ${data?.pokemon?.name}! Give them a name!`}
      >
        <div>
          <input
            type="text"
            value={nickname}
            onChange={(e) => setNickname(e.target.value)}
          />
          <button disabled={!nickname} onClick={handleSuccess}>
            Finish
          </button>
        </div>
      </Modals>
    </div>
  );
}
