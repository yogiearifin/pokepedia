import { css } from "@emotion/css";

export const containerDetail = css`
  font-family: "Exo", sans-serif;
  display: flex;
  flex-direction: column;
  h1 {
    text-transform: capitalize;
  }
  .container-image {
    display: flex;
    justify-content: center;
    background-color: #f4f4f4;
    margin-bottom: 10px;
  }
  .container-button {
    margin: 20px 0;
    display: flex;
    justify-content: center;
  }
  button {
    border: none;
    border-radius: 15px;
    font-size: 24px;
    background-color: #ed1b24;
    color: white;
    width: fit-content;
    padding: 10px;
  }
  .pokemon-list-vertical {
    margin: 10px 0;
    display: flex;
    justify-content: space-evenly;
  }
`;
