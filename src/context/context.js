import { createContext, useState, useEffect } from "react";
import { useLazyQuery } from "@apollo/client";
import { getPokemon } from "../graphql/pokemon";

export const PokeContext = createContext();

const initialState = {
  listPokemon: [],
  loading: false,
  catchList: JSON.parse(localStorage.getItem("catchList")),
  limit: 20,
  offset: 0,
};

export const DataProvider = (props) => {
  const [pokemonData, setPokemonData] = useState(initialState);
  const getCatchList = () => {
    setPokemonData({
      ...pokemonData,
      catchList: JSON.parse(localStorage.getItem("catchList")),
    });
  };
  const setCatchList = (pokemon) => {
    setPokemonData({
      ...pokemonData,
      catchList: pokemon,
    });
    localStorage.setItem("catchList", JSON.stringify(pokemon));
  };

  const removePokemon = (nick) => {
    let newlist = [...pokemonData.catchList];
    const filter = newlist.filter((item) => item.nickname !== nick);
    setCatchList(filter);
  };

  // const { data, loading, error } = useQuery(getPokemon, {
  //   variables: { limit: initialState.limit, offset: initialState.offset },
  // });
  const [getPokemonList, { loading, data, error }] = useLazyQuery(getPokemon, {
    variables: { limit: pokemonData.limit, offset: pokemonData.offset },
  });
  useEffect(() => {
    setPokemonData({
      ...pokemonData,
      listPokemon: data,
      loading: loading,
      error: error || null,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading, data]);

  const setOffset = (num) => {
    setPokemonData({
      ...pokemonData,
      offset: num,
    });
  };

  return (
    <PokeContext.Provider
      value={{
        setPokemonData,
        getCatchList,
        setCatchList,
        pokemonData,
        setOffset,
        getPokemonList,
        removePokemon,
      }}
    >
      {props.children}
    </PokeContext.Provider>
  );
};
