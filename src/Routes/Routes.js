import { Routes, Route } from "react-router-dom";
import DetailPokemon from "../pages/DetailPokemon";
import ListPokemon from "../pages/ListPokemon";
import MyPokemon from "../pages/MyPokemon";
import Navbar from "../components/navbar";

const PokemonRoute = () => {
  return (
    <>
      <Navbar />
      <Routes>
        <Route path="/" exact element={<ListPokemon />} />
        <Route path="/pokemon/:name" exact element={<DetailPokemon />} />
        <Route path="/mypokemon" exact element={<MyPokemon />} />
      </Routes>
    </>
  );
};

export default PokemonRoute;
