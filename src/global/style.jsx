import { css } from "@emotion/css";

export const global = css`
  h1,
  h2,
  h3,
  h4,
  h5 {
    margin: 0;
  }
`;

export const container = css`
  @media (min-width: 900px) {
    padding: 10% 10% 0 10%;
  }

  padding: 30% 10% 0 10%;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;

export const containerMedium = css`
  @media (min-width: 900px) {
    padding: 10% 5% 0 5%;
  }

  padding: 30% 5% 0 5%;
  width: 100%
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  h1,h3,h5 {
    text-align: center;
  }
`;
